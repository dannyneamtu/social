<?php

use App\Tag;
use App\Book;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $authors = DB::table('authors')->pluck('id')->ToArray();
        $limit = 10;

        $tags = DB::table('tags')->pluck('id')->ToArray();
        $last = count($tags) - 1;

        for ($i = 0; $i < $limit; $i++) {
            $book = Book::create([
                'name' => $faker->word,
                'description' => $faker->sentence,
                'author_id' => $authors[array_rand($authors)],
                'created_at' => $faker->dateTime($max = 'now'),
                'updated_at' => $faker->dateTime($max = 'now'),
            ]);

            if (count($tags))
            {
                $book->tags()->attach( $tags[ rand(0, $last ) ] );
            }
        }
    }
}
