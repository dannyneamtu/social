<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind('App\Repositories\TagsRepository\TagsRepositoryInterface',
            'App\Repositories\TagsRepository\TagsRepository');

        $this->app->bind('App\Repositories\AuthorsRepository\AuthorsRepositoryInterface',
            'App\Repositories\AuthorsRepository\AuthorsRepository');

        $this->app->bind('App\Repositories\BooksRepository\BooksRepositoryInterface',
            'App\Repositories\BooksRepository\BooksRepository');
    }
}
