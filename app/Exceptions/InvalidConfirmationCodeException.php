<?php
/**
 * Created by PhpStorm.
 * User: dani
 * Date: 06.02.2017
 * Time: 12:43
 */

namespace App\Exceptions;


use Exception;

class InvalidConfirmationCodeException extends Exception
{

    /**
     * @var array
     */
    protected $errors;

    /**
     * @param array $errors
     */
    public function __construct($errors = [])
    {
        parent::__construct();

        $this->errors = $errors;
    }

    /**
     * @return array
     */
    public function getErrorMessages()
    {
        return $this->errors;
    }

}