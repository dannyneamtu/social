<?php

namespace App\Repositories\AuthorsRepository;

use App\Repositories\DbRepository;


class AuthorsRepository extends DbRepository implements AuthorsRepositoryInterface{

    protected $modelName = 'App\Author';

}