<?php

namespace App\Repositories\BooksRepository;

use App\Repositories\DbRepository;


class BooksRepository extends DbRepository implements BooksRepositoryInterface{

    protected $modelName = 'App\Book';
    
}