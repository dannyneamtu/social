<?php

namespace App\Repositories\TagsRepository;

use App\Repositories\DbRepository;


class TagsRepository extends DbRepository implements TagsRepositoryInterface{

    protected $modelName = 'App\Tag';

}