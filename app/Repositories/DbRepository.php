<?php

namespace App\Repositories;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


abstract class DbRepository {

    protected $modelName;

    public function with($relations) {
        if (is_string($relations)) $relations = func_get_args();

        $this->with = $relations;

        return $this;
    }

    public function all()
    {
        $instance = $this->getNewInstance();
        return $instance->all();
    }

    public function paginate( $count )
    {
        $instance = $this->getNewInstance();

        return $instance->paginate( $count );
    }

    public function find( $id )
    {
        $instance = $this->getNewInstance();
        return $instance->find( $id );
    }

    public function store( $data )
    {
        $instance = $this->getNewInstance();
        return $instance->create( $data );
    }

    public function update( $id, $data )
    {
        $instance = $this->getNewInstance();
        return $instance->find( $id )->update( $data );
    }

    public function delete( $id )
    {
        $instance = $this->getNewInstance();
        return $instance->destroy( $id );
    }

    public function findBy( $field, $value )
    {
        $instance = $this->getNewInstance();
        return $instance->where( $field, $value )->firstOrFail();
    }

    public function findAllBy( $field, $value )
    {
        $instance = $this->getNewInstance();
        return $instance->where( $field, $value )->get();
    }

    public function selectDistinct(  $field )
    {
        $instance = $this->getNewInstance();
        return $instance->select( $field )->distinct()->get();
    }

    public function findLike( $field, $value )
    {
        $instance = $this->getNewInstance();
        return $instance->where( $field, 'LIKE', '%'.$value.'%' )->get();
    }

    public function getFirstRecord()
    {
        $instance = $this->getNewInstance();
        return $instance->orderBy( 'id', 'asc' )->take(1)->get();
    }

    public function getLastRecord()
    {
        $instance = $this->getNewInstance();
        return $instance->orderBy( 'id', 'desc' )->take(1)->get();
    }

    public function findFirst()
    {
        $instance = $this->getNewInstance();
        return $instance->where('id', '>', 0)->firstOrFail();
    }

    public function getAllOrderedBy( $field, $type) {
        $instance = $this->getNewInstance();
        return $instance->orderBy( $field, $type)->get();
    }

    protected function getNewInstance()
    {
        $model = $this->modelName;
        return new $model;
    }
    
}