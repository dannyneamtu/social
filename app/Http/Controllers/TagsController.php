<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use App\Http\Requests\TagRequest;
use App\Http\Requests;
use App\Repositories\TagsRepository\TagsRepositoryInterface;
use Illuminate\Http\Response;

class TagsController extends Controller
{
    protected $tags;

    public function __construct( TagsRepositoryInterface $tags)
    {
        $this->tags = $tags;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tags = $this->tags->all();

        return view('tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view( 'tags.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TagRequest|Request $request
     * @return Response
     */
    public function store(TagRequest $request)
    {
        $this->tags->store( $request->all() );

        flash('Tagul a fost creat cu succes!')->important();

        return redirect()->route('tags.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Tag $tag
     * @return Response
     * @internal param int $id
     */
    public function edit(Tag $tag)
    {
        return view('tags.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TagRequest|Request $request
     * @param Tag $tag
     * @return Response
     * @internal param int $id
     */
    public function update(TagRequest $request, Tag $tag)
    {

        $this->tags->update( $tag->id , $request->only('name', 'description') );

        flash()->success('Tagul a fost modificata cu succes!');

        return redirect()->route('tags.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tag $tag
     * @return Response
     * @internal param int $id
     */
    public function destroy(Tag $tag)
    {
        $this->tags->delete( $tag->id );

        return redirect()->route('tags.index');
    }

    public function confirmDestroy(Tag $tag) {

        return view('tags.confirm-destroy', compact('tag'));
    }
}
