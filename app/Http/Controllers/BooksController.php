<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use App\Repositories\BooksRepository\BooksRepositoryInterface;
use App\Tag;
use Illuminate\Http\Request;
use App\Http\Requests\BookRequest;
use App\Http\Requests;
use Illuminate\Http\Response;

class BooksController extends Controller
{

    protected $books;

    /**
     * BooksController constructor.
     * @param BooksRepositoryInterface $books
     */
    public function __construct(BooksRepositoryInterface $books)
    {
        $this->books = $books;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $books = $this->books->with('author', 'tags')->paginate(5);

        //$books = Book::with('author')->with('tags')->paginate(5);

        return view('books.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $tags = Tag::all()->pluck('id','name');

        return view( 'books.create', compact('tags', 'authors') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BookRequest|Request $request
     * @return Response
     */
    public function store(BookRequest $request)
    {
        $this->books->store( $request->all() );

        //flash('Bookul a fost creat cu succes!')->important();

        return redirect()->route('books.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Book $book
     * @return Response
     * @internal param int $id
     */
    public function edit(Book $book)
    {
        $authors = Author::all()->pluck('name','id')->ToArray();

        $tags = Tag::all()->pluck('name','id')->ToArray();

        return view('books.edit', compact('book', 'authors', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BookRequest|Request $request
     * @param Book $book
     * @return Response
     * @internal param int $id
     */
    public function update(BookRequest $request, Book $book)
    {

        $this->books->update( $book->id , $request->only('name', 'description') );

        //flash()->success('Bookul a fost modificata cu succes!');

        return redirect()->route('books.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Book $book
     * @return Response
     * @internal param int $id
     */
    public function destroy(Book $book)
    {
        $this->books->delete( $book->id );

        return redirect()->route('books.index');
    }

    public function confirmDestroy(Book $book) {

        return view('books.confirm-destroy', compact('book'));
    }
}
