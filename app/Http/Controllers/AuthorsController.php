<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Author;
use App\Http\Requests\AuthorRequest;
use App\Http\Requests;
use App\Repositories\AuthorsRepository\AuthorsRepositoryInterface;
use Illuminate\Http\Response;

class AuthorsController extends Controller
{
    protected $authors;

    public function __construct( AuthorsRepositoryInterface $authors)
    {
        $this->authors = $authors;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $authors = $this->authors->paginate(10);

        return view('authors.index', compact('authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view( 'authors.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AuthorRequest|Request $request
     * @return Response
     */
    public function store(AuthorRequest $request)
    {
        $this->authors->store( $request->all() );

        flash('Authorul a fost creat cu succes!')->important();

        return redirect()->route('authors.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Author $author
     * @return Response
     * @internal param int $id
     */
    public function edit(Author $author)
    {
        return view('authors.edit', compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AuthorRequest|Request $request
     * @param Author $author
     * @return Response
     * @internal param int $id
     */
    public function update(AuthorRequest $request, Author $author)
    {

        $this->authors->update( $author->id , $request->only('name', 'description') );

        flash()->success('Authorul a fost modificata cu succes!');

        return redirect()->route('authors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Author $author
     * @return Response
     * @internal param int $id
     */
    public function destroy(Author $author)
    {
        $this->authors->delete( $author->id );

        return redirect()->route('authors.index');
    }

    public function confirmDestroy(Author $author) {

        return view('authors.confirm-destroy', compact('author'));
    }
}
