<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'Required|Min:3|Max:100|regex:/^[a-z0-9 .\-]+$/i',
            'description' => 'required|regex:/^[a-z0-9 .\-]+$/i'
        ];
    }
}
