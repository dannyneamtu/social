@extends('layouts.app')

@section('content')
<div class="container">
   <div class="row" >
    <h2>Add new book</h2>
    {!! Form::open( ['route' => 'books.store', 'files'=>true]) !!}

    @include( 'books._form')

    {!! Form::close() !!}
   </div>
</div>
    <script type="text/javascript">
        window.onload = function () {
            CKEDITOR.replace('content', {
                "filebrowserBrowseUrl": "{{ url('filemanager/show') }}"
            });
            if(window.CKEDITOR){
                CKEDITOR.on('instanceCreated', function (ev) {
                    CKEDITOR.config['autoParagraph'] = false;
                    CKEDITOR.dtd.$removeEmpty['span'] = 0;
                    CKEDITOR.dtd.$removeEmpty['script'] = 0;
                });
            }
        };
    </script>

@stop