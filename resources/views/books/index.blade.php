@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row" >
            <h1>Books</h1>
            <div class="pull-left">
                <div class="btn-toolbar">
                    <a href="{!! route('books.create') !!}" class="btn btn-primary">
                        <span class="glyphicon glyphicon-plus"></span>Add New</a>
                </div>
            </div>
            <br> <br> <br>
            @if($books->count())
                <div class="table-responsive" style="padding-bottom: 150px">
                    <table class="table table-striped" style="min-height: 300px;" id="booksTable">
                        <thead>
                        <tr>
                            <th>Cover</th>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Tags</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $books as $book )
                            <tr>
                                <td>
                                    @if( $book->cover_image == '')
                                        {!! Html::image('images/Default.png', $alt='$book->name', $attributes = array('width' => '20')) !!}
                                    @endif
                                    {!! $book->cover_image !!}</td>
                                <td>{!! $book->name !!}</td>
                                <td>{!! $book->author->name !!}</td>
                                <td>
                                    @unless ( $book->tags->isEmpty() )
                                        <ul class="list-inline">
                                            @foreach($book->tags as $tag)
                                                <li>{!! $tag->name !!} </li>
                                            @endforeach
                                        </ul>
                                    @endunless
                                </td>
                                <td>{!! $book->description !!}</td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="#">
                                            Action <span class="caret"></span> </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{!! route('books.edit', array($book->id)) !!}">
                                                    <span class="glyphicon glyphicon-edit"></span>Edit
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="{!! URL::route('books.delete', array($book->id)) !!}">
                                                    <span class="glyphicon glyphicon-remove-circle"></span>Delete
                                                    </a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="#">
                                                    <span class="glyphicon glyphicon-remove-circle"></span>Rent
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="alert alert-danger">No results found</div>
            @endif

            <div class="row">
                <div class="col-md-12 text-center">
                    {!! $books->links() !!}
                </div>
            </div>

        </div>
    </div>
@stop


