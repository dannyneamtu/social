
<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
    {!! FORM::label('name', 'Title:') !!}
    {!! FORM::text('name', null,  ['class' => 'form-control', 'id' => 'book_title' ]) !!}
    {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
</div>

<!-- Content -->
<div class="control-group {{ $errors->has('description') ? 'has-error' : '' }}">
    {!! FORM::label('description', 'Continut:') !!}

    <div class="form-group">
        {!! Form::textarea('description', null, array('class'=>'form-control', 'id' => 'description', 'placeholder'=>'Description', 'value'=>Request::old('description'))) !!}
        {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group">
    {!! FORM::label('author_id', 'Author:') !!} <br>
    {!! Form::select('author_id', $authors, null,  ['class' => 'form-control']) !!}
    {!! $errors->first('author_id', '<span class="help-block">:message</span>') !!}
</div>

<!-- Page_image -->
<div class="form-group">
    @if( isset( $book ) && $book->cover_image != '')
        {!! FORM::label('cover_image', 'Poza reprezentativa pt pagina:') !!}
        <br>
        {!! Html::image('cover_images/'.$book->cover_image, $alt='$book->title', $attributes = array('width' => '50')) !!}
        <br>
        <a href="javascript:deletePageImg('{{ $book->id }}');">Delete</a>
    @else
        {!! Form::label('cover_image', 'Poza reprezentativa pt pagina:') !!}
        {!! Form::file('cover_image') !!}
    @endif
</div>

<!-- Tags -->
<div class="form-group">
    {!! Form::label('tag_list', 'Tags') !!}
    {!! Form::select('tag_list[]', $tags, null, ['id' => 'tag_list', 'class' => 'form-control', 'multiple'] ) !!}
</div>

<div class="form-group">
    {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
    {!! link_to(URL::previous(), 'Cancel', ['class' => 'btn btn-default']) !!}
</div>
