@extends('layouts.app')

@section('content')
{!! Html::script('ckeditor/ckeditor.js') !!}
<div class="container">
    <div class="row" >
        <h2>{{ $book->title }}</h2>
        {!! Form::model($book, ['route' => ['books.update', $book->id], 'method' => 'PATCH','files'=>true ] ) !!}

        {!! FORM::hidden('id', $book->id,  ['class' => 'form-control']) !!}

        @include( 'books._form')

        {!! Form::close() !!}
    </div>
</div>
    <script type="text/javascript">

        function deletePageImg(id) {

            if (confirm('Are you sure you want to delete this?')) {
                $.ajax({
                    type: "GET",
                    url: baseUrl + '/books/' + id + '/deletebookimg', //resource
                    success: function(affectedRows) {
                        //if something was deleted, we redirect the user to the users book, and automatically the user that he deleted will disappear
                        if (affectedRows > 0) window.location = baseUrl + '/books/' + id + '/edit';
                    }
                });
            }
        }
/*
        window.onload = function () {
            CKEDITOR.replace('content', {
                "filebrowserBrowseUrl": "{{ url('filemanager/show') }}"
            });
            if(window.CKEDITOR){
                CKEDITOR.on('instanceCreated', function (ev) {
                    CKEDITOR.config['autoParagraph'] = false;
                    CKEDITOR.dtd.$removeEmpty['span'] = 0;
                    CKEDITOR.dtd.$removeEmpty['script'] = 0;
                });
            }
        };
        */
    </script>

@stop