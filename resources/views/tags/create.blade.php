@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" >

        <h2>Add new Tag</h2>
        {!! Form::open( ['route' => 'tags.store']) !!}

            @include( 'tags._form')

        {!! Form::close() !!}

    </div>
</div>
@stop