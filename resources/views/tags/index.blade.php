@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row" >
            <h1>Tags</h1>
            <div class="pull-left">
                <div class="btn-toolbar">
                    <a href="{!! route('tags.create') !!}" class="btn btn-primary">
                        <span class="glyphicon glyphicon-plus"></span>Add New</a>
                </div>
            </div>
            <br> <br> <br>
            @if($tags->count())
                <div class="table-responsive" style="padding-bottom: 150px">
                    <table class="table table-striped" style="min-height: 300px;" id="tagsTable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $tags as $tag )
                            <tr>
                                <td>{!! $tag->name !!}</td>
                                <td>{!! $tag->description !!}</td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="#">
                                            Actiune <span class="caret"></span> </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{!! route('tags.edit', array($tag->id)) !!}">
                                                    <span class="glyphicon glyphicon-edit"></span>Edit
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="{!! URL::route('tags.delete', array($tag->id)) !!}">
                                                    <span class="glyphicon glyphicon-remove-circle"></span>Delete
                                                    </a>
                                            </li>

                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="alert alert-danger">No results found</div>
            @endif

        </div>
    </div>
@stop


