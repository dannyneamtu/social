@extends('layouts.app')
@section('content')
<section class="content-header">
    <h1> Tag
        <small> | Delete Tag</small>
    </h1>

</section>
<br>
<br>
<br>
<div class="container">

    {!! Form::open( [ 'route' => ['tags.destroy', $tag->id ], 'method' => 'DELETE' ] ) !!}
    {!! Form::hidden( '_method', 'DELETE' ) !!}
    <div class="alert alert-warning">
        <div class="pull-left"><b> Be Careful!</b> Are you sure you want to delete <b>{!! $tag->name !!} </b> ?
        </div>
        <div class="pull-right">
            {!! Form::submit( 'Yes', array( 'class' => 'btn btn-danger' ) ) !!}
            {!! link_to( URL::previous(), 'No', array( 'class' => 'btn btn-primary' ) ) !!}
        </div>
        <div class="clearfix"></div>
    </div>
    {!! Form::close() !!}
</div>
@stop