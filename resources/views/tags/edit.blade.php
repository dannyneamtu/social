@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row" >
            <h2>{{ $tag->name }}</h2>
            {!! Form::model($tag, ['route' => ['tags.update', $tag->id], 'method' => 'PATCH' ] ) !!}

            {!! FORM::hidden('id', $tag->id,  ['class' => 'form-control']) !!}

                @include( 'tags._form')

            {!! Form::close() !!}
        </div>
    </div>
@stop