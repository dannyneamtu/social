@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row" >
            <h1>Authors</h1>
            <div class="pull-left">
                <div class="btn-toolbar">
                    <a href="{!! route('authors.create') !!}" class="btn btn-primary">
                        <span class="glyphicon glyphicon-plus"></span>Add New</a>
                </div>
            </div>
            <br> <br> <br>
            @if($authors->count())
                <div class="table-responsive" style="padding-bottom: 150px">
                    <table class="table table-striped" style="min-height: 300px;" id="authorsTable">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $authors as $author )
                            <tr>
                                <td>{!! $author->name !!}</td>
                                <td>{!! $author->description !!}</td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="#">
                                            Actiune <span class="caret"></span> </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{!! route('authors.edit', array($author->id)) !!}">
                                                    <span class="glyphicon glyphicon-edit"></span>Editeaza
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="{!! URL::route('authors.delete', array($author->id)) !!}">
                                                    <span class="glyphicon glyphicon-remove-circle"></span>Sterge
                                                    </a>
                                            </li>

                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="alert alert-danger">No results found</div>
            @endif

            <div class="row">
                <div class="col-md-12 text-center">
                    {!! $authors->links() !!}
                </div>
            </div>
        </div>
    </div>
@stop


