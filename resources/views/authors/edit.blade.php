@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row" >
            <h2>{{ $author->name }}</h2>
            {!! Form::model($author, ['route' => ['authors.update', $author->id], 'method' => 'PATCH' ] ) !!}

            {!! FORM::hidden('id', $author->id,  ['class' => 'form-control']) !!}

                @include( 'authors._form')

            {!! Form::close() !!}
        </div>
    </div>
@stop