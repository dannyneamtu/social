@extends('layouts.app')
@section('content')
    <div class="container">
        <section class="content-header">
            <h1> Author
                <small> | Delete Author</small>
            </h1>

        </section>
        <br>
        <br>
        <br>

        {!! Form::open( [ 'route' => ['authors.destroy', $author->id ], 'method' => 'DELETE' ] ) !!}
        {!! Form::hidden( '_method', 'DELETE' ) !!}
        <div class="alert alert-warning">
            <div class="pull-left"><b> Be Careful!</b> Are you sure you want to delete <b>{!! $author->name !!} </b> ?
            </div>
            <div class="pull-right">
                {!! Form::submit( 'Yes', array( 'class' => 'btn btn-danger' ) ) !!}
                {!! link_to( URL::previous(), 'No', array( 'class' => 'btn btn-primary' ) ) !!}
            </div>
            <div class="clearfix"></div>
        </div>
        {!! Form::close() !!}
    </div>
@stop