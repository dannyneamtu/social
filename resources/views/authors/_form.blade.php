

    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
        {!! FORM::label('name', 'Author:') !!}
        {!! FORM::text('name', null,  ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
    </div>

    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
        {!! FORM::label('description', 'Description:') !!}
        {!! FORM::text('description', null,  ['class' => 'form-control']) !!}
        {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
    </div>

    <div class="form-group">
        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        {!! link_to(URL::previous(), 'Cancel', ['class' => 'btn btn-default']) !!}
    </div>
