@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" >

        <h2>Add new Author</h2>
        {!! Form::open( ['route' => 'authors.store']) !!}

            @include( 'authors._form')

        {!! Form::close() !!}

    </div>
</div>
@stop