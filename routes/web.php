<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\View;

Route::bind('tags', function($id) {
    return App\Tag::whereId($id)->firstOrFail();
});

Route::bind('authors', function($id) {
    return App\Author::whereId($id)->firstOrFail();
});

Route::bind('books', function($id) {
    return App\Book::whereId($id)->firstOrFail();
});

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

//tags
Route::resource( 'tags', 'TagsController' );
Route::get('tags/{tags}/delete', array('as'   => 'tags.delete',
    'uses' => 'TagsController@confirmDestroy'));

//authors
Route::resource( 'authors', 'AuthorsController' );
Route::get('authors/{authors}/delete', array('as'   => 'authors.delete',
    'uses' => 'AuthorsController@confirmDestroy'));

//books
Route::resource( 'books', 'BooksController' );
Route::get('books/{books}/delete', array('as'   => 'books.delete',
    'uses' => 'BooksController@confirmDestroy'));

Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'Auth\RegisterController@confirm'
]);

// filemanager - pt ckeditor
Route::get('filemanager/show', function () {

    return View::make('backend/plugins/filemanager');
});
